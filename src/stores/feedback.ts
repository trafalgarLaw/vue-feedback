import { defineStore } from "pinia";

export const useFeedBackStore = defineStore({
    id: 'feedback',
    state: () => ({
        feedbacks: [],
    }),
    getters: {
        getNumberFeedbacks: (state) => {
            return state.feedbacks.length;
        },

        average: (state) => {
            const reduce = state.feedbacks.reduce((a, {rating}) => {
                return a + rating;
            }, 0);
            
            const average = reduce / state.feedbacks.length;
            return average.toFixed(2);
        }
    },
    actions: {
        deleteFeedback(id: number) {
            this.feedbacks = this.feedbacks.filter(feedback => feedback.id !== id);
            //---- it work also ----
            //const feedbackFilter = this.feedbacks.filter(feedback => feedback.id !== id);
            //return this.feedbacks = [...feedbackFilter];
        }
    }
})