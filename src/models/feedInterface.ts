export interface FeedInterface {
    id?: number;
    rating: number;
    feed: string;
    date?: Date;
}